//
//  AppDelegate.h
//  my_2048
//
//  Created by 米拉 on 2017/2/2.
//  Copyright © 2017年 米拉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

