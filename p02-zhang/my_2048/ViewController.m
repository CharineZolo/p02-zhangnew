//
//  ViewController.m
//  my_2048
//
//  Created by 米拉 on 2017/2/2.
//  Copyright © 2017年 米拉. All rights reserved.
//

#import "ViewController.h"

@interface ViewController()

@end

@implementation ViewController

@synthesize t00,t01,t02,t03,
t10,t11,t12,t13,
t20,t21,t22,t23,
t30,t31,t32,t33,sum,gameover,tryagain,score,poster,best,
up,down,left,right;

@synthesize tiles;




- (void)viewDidLoad {

    [super viewDidLoad];
    int my2Int=2;
    int my4Int=4;

    [tryagain setHidden:YES];
    [score setHidden:YES];
    [gameover setHidden:YES];

    tiles = [[NSMutableArray alloc ] init];
    [tiles insertObject:t00 atIndex:0];
    [tiles insertObject:t01 atIndex:1];
    [tiles insertObject:t02 atIndex:2];
    [tiles insertObject:t03 atIndex:3];
    [tiles insertObject:t10 atIndex:4];
    [tiles insertObject:t11 atIndex:5];
    [tiles insertObject:t12 atIndex:6];
    [tiles insertObject:t13 atIndex:7];
    [tiles insertObject:t20 atIndex:8];
    [tiles insertObject:t21 atIndex:9];
    [tiles insertObject:t22 atIndex:10];
    [tiles insertObject:t23 atIndex:11];
    [tiles insertObject:t30 atIndex:12];
    [tiles insertObject:t31 atIndex:13];
    [tiles insertObject:t32 atIndex:14];
    [tiles insertObject:t33 atIndex:15];

    [sum setText:@"0"];
    sum.tag=0;
    [best setText:@"Best:00000"];
    best.tag=0;

    UILabel * label1;
    for(label1 in tiles){

        label1.tag=0;
        [label1 setText:@" "];

    }
    [t00 setText:[NSString stringWithFormat:@"%d",my2Int]];
    [t01 setText:[NSString stringWithFormat:@"%d",my4Int]];
    t00.tag=2;
    t01.tag=4;
    //[t02 setText:[NSString stringWithFormat:@"%ld",(long)t21.tag]];

    // Do any additional setup after loading the view, typically from a nib.
}
- (void) randomize{

    int i=0;int count=0,mark=0;
    UILabel *label1;
    if(sum.tag>best.tag)
        best.tag=sum.tag;
    [best setText:[NSString stringWithFormat:@"Best :%ld",(long)best.tag]];

   // UIColor *graycolor1 = [UIColor colorWithRed:(255.0/255.0) green:(50.0/255.0) blue:(50.0/255.0) alpha:(100.0/100.0)];
    UIColor * color2 = [UIColor colorWithRed:(245.0/255.0) green:(245.0/255.0) blue:(245.0/255.0) alpha:(100.0/100.0)];
    UIColor * color4 = [UIColor colorWithRed:(255.0/255.0) green:(248.0/255.0) blue:(220.0/255.0) alpha:(100.0/100.0)];

    UIColor * redcolor32 = [UIColor colorWithRed:(255.0/255.0) green:(99.0/255.0) blue:(71.0/255.0) alpha:(100.0/100.0)];
    UIColor * brown8 = [UIColor colorWithRed:(244.0/255.0) green:(164.0/255.0) blue:(96.0/255.0) alpha:(100.0/100.0)];
    UIColor * orange16 = [UIColor colorWithRed:(255.0/255.0) green:(97.0/255.0) blue:(0.0/255.0) alpha:(100.0/100.0)];

    for(label1 in tiles){
        if(label1.tag==2048)
            [gameover setText:@"You Win!"];

        
        if(label1.tag==0)
            count++;
    }
    if(count==0) {
      //  NSLog(@"Game over");
        // EXIT_FAILURE;
        left.hidden = YES;
        right.hidden = YES;
        up.hidden = YES;
        down.hidden = YES;
        poster.hidden = YES;
        sum.hidden = YES;
        best.hidden =YES;
        //[ setHidden:YES];
       



        [gameover setHidden:NO];
        [tryagain setHidden:NO];
        [score setHidden:NO];
        [score setText:[NSString stringWithFormat:@"Your Score is :%ld",(long)sum.tag]];

        for(label1 in tiles){
            
            label1.hidden=YES;}

        return;

    }

    NSLog(@"count=%d",count);
    i=arc4random()%count;
    NSLog(@"i2=%d",i);
    count=0;
    label1=t00;
//====================================

    for(label1 in tiles){

      switch(label1.tag){
        case 0:{
          label1.textColor = [UIColor blackColor];
            label1.backgroundColor = [UIColor lightGrayColor];
        };break;
        case 2:{
          label1.textColor = [UIColor blackColor];
          [label1 setBackgroundColor:color2];

        };break;
        case 4:{
          label1.textColor = [UIColor blackColor];
          [label1 setBackgroundColor:color4];

        };break;
        case 8:{
          label1.textColor = [UIColor whiteColor];
          [label1 setBackgroundColor:brown8];

        };break;
        case 16:{
          label1.textColor = [UIColor whiteColor];
          [label1 setBackgroundColor:orange16];
        };break;
        case 32:{
          label1.textColor = [UIColor whiteColor];
          [label1 setBackgroundColor:redcolor32];
        };break;
        case 64:{
          label1.textColor = [UIColor whiteColor];
          label1.backgroundColor = [UIColor redColor];
        };break;
        case 128:{
          label1.textColor = [UIColor whiteColor];
          label1.backgroundColor = [UIColor yellowColor];
        };break;
        case 256 :{
          label1.textColor = [UIColor whiteColor];
          label1.backgroundColor = [UIColor yellowColor];
        };break;
        case 512 :{
          label1.textColor = [UIColor whiteColor];
            label1.backgroundColor = [UIColor yellowColor];
        };break;
        case 1024 :{
          label1.textColor = [UIColor whiteColor];
          label1.backgroundColor = [UIColor yellowColor];
        };break;
        case 2048 :{
          label1.textColor = [UIColor whiteColor];
          label1.backgroundColor = [UIColor yellowColor];
        };break;

      }
        if(label1.tag==0){

            if(count==i&&mark==0){
                i=arc4random()%2;
                if(i==0&&label1.tag==0){
                    [label1 setText:@"2"];
                    label1.tag=2;
                    label1.textColor = [UIColor blackColor];
                    [label1 setBackgroundColor:color2];
                    mark=1;

                }

                else{
                    [label1 setText:@"4"];
                    label1.tag=4;
                    label1.textColor = [UIColor blackColor];
                    [label1 setBackgroundColor:color4];
                    mark=1;


                };//break;
            }
            count++;
        }


    }



}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)TryAgain:(id)sender {


    UILabel * label1=t00;

    for(label1 in tiles){

        label1.tag=0;
        [label1 setText:@""];

          //label1.textColor = [UIColor blackColor];
          label1.backgroundColor = [UIColor lightGrayColor];


    }
    poster.hidden = NO;
    left.hidden = NO;
    right.hidden = NO;
    up.hidden = NO;
    down.hidden = NO;
    sum.hidden =NO;
    sum.tag=0;
    [sum setText:@" "];
    best.hidden = NO;


    [gameover setHidden:YES];
    [tryagain setHidden:YES];
    [score setHidden:YES];
    [gameover setText:@"Game Over!"];

    for(label1 in tiles){

        label1.hidden=NO;}
    //[t00 setText:@"2"];
    //[t13 setText:@"4"];
    //t00.tag=2;
    //t13.tag=4;
    [self randomize];
    [self randomize];




}

-(IBAction)moveLeft:(id)sender{
  int i=0,j=0,k=0;
  UILabel * label1=t00;
  long int array [4][4];

  for(label1 in tiles){

      array[i][j]=label1.tag;
      j++;
      if(j%4==0){i++;j=0;}

  }

  for(i=0;i<4;i++){
    for(j=0;j<4;j++){
    //  printf("arr[%d][%d]=%ld\n",i,j,array[i][j]);
      //  printf("arr[%d][%d]+1=%ld\n",i,j,array[i][j+1]);

      if(array[i][j]==0){
        if(j!=3&&array[i][j+1]!=0){
          k=j;
          while(k>=0&&array[i][k]==0){
            array[i][k]=array[i][k+1];
            array[i][k+1]=0;
            k--;
          }
          //array[i][k]=array[i][j+1];
          //array[i][j+1]=0;
        }
      }

      if(j<3&&array[i][j+1]==array[i][j]&&array[i][j]!=0){
        array[i][j]=2*array[i][j+1];
        array[i][j+1]=0;
      //  printf("arr[%d][%d]=%ld\n",i,j,array[i][j]);
        //  printf("arr[%d][%d]+1=%ld\n",i,j,array[i][j+1]);

        sum.tag+=array[i][j];
        [sum setText:[NSString stringWithFormat:@"%ld",(long)sum.tag]];
        /*k=j;
        while(k>0&&array[i][k-1]!=0){
          array[i][k]=array[i][k-1];
          array[i][k-1]=0;
          k--;
        }*/
                 //array[i][j]=0;
          // [sum setText:[NSString stringWithFormat:@"%ld",(long)sum.tag]];


      }
    }


  }
  i=0;j=0;
      for(label1 in tiles){
        label1.tag =  array[i][j];
        // NSLog(@"%d,arr=%ld",i,array[i][j]);

         // NSLog(@"%d,tag=%ld",i,label1.tag);
          if(label1.tag!=0)
              [label1 setText:[NSString stringWithFormat:@"%ld",(long)label1.tag]];
          else
              [label1 setText:@""];

          j++;
          if(j%4==0){i++;j=0;}

      }


    [self randomize];




}





-(IBAction)moveright:(id)sender{

int i=0,j=0,k=0;
UILabel * label1=t00;
long int array [4][4];

for(label1 in tiles){


    array[i][j]=label1.tag;


    j++;
    if(j%4==0){i++;j=0;}


}
for(i=0;i<4;i++){
  for(j=0;j<4;j++){
    if(j<3&&array[i][j]!=0&&array[i][j+1]==0){
         array[i][j+1]=array[i][j];
         array[i][j]=0;
         k=j;
         while(k>0&&array[i][k-1]!=0){
           array[i][k]=array[i][k-1];
           array[i][k-1]=0;
            NSLog(@"%d,arr=%ld",k,array[i][k]);

           k--;

         }

         //array[k+1][i]=0;


    }
    if(j<3&&array[i][j]==array[i][j+1]&&array[i][j]!=0){
        array[i][j+1]=2*array[i][j];

        sum.tag+=array[i][j+1];
        [sum setText:[NSString stringWithFormat:@"%ld",(long)sum.tag]];

        array[i][j]=0;
        k=j;
        while(k>0&&array[i][k-1]!=0){
            array[i][k]=array[i][k-1];
          array[i][k-1]=0;
          k--;

        }
        //array[i][k]=0;

  }

}
}
//=============================setText for label================================
i=0;j=0;
for(label1 in tiles){
  label1.tag =  array[i][j];

    if(label1.tag!=0)
        [label1 setText:[NSString stringWithFormat:@"%ld",(long)label1.tag]];
    else
        [label1 setText:@""];

    j++;
    if(j%4==0){i++;j=0;}

}

//===========================give a random 2 or 4 for label=====================
    [self randomize];

}

-(IBAction)moveup:(id)sender{

    int i=0,j=0,k=0;
    UILabel * label1=t00;
    long int array [4][4];

    for(label1 in tiles){


        array[i][j]=label1.tag;
       // printf("arr[%d][%d]=%ld\n",i,j,array[i][j]);


        j++;
        if(j%4==0){i++;j=0;}


    }


    for(i=0;i<4;i++){
        for(j=0;j<4;j++){

          printf("arr[%d][%d]=%ld\n",i,j,array[j][i]);
          printf("arr[%d][%d]+1=%ld\n",i,j,array[j+1][i]);

          if(array[j][i]==0){

            if(j!=3&&array[j+1][i]!=0){
              k=j;
                while(k>=0&&array[k][i]==0){
                  array[k][i]=array[k+1][i];
                  array[k+1][i]=0;
                   k--;

                }


            //  array[k+1][i]=array[j+1][i];
              //array[j+1][i]=0;


            }



          }
          if(j<3&&array[j][i]==array[j+1][i]&&array[j][i]!=0){
            array[j][i]=2*array[j+1][i];
            array[j+1][i]=0;
            sum.tag+=array[j][i];
            [sum setText:[NSString stringWithFormat:@"%ld",(long)sum.tag]];
          /*  k=j;
            while(k<3&&array[k+1][i]!=0){
              array[k][i]=array[k+1][i];
              array[k+1][i]=0;
              k++;

            }*/

          //  array[j][i]=0;


          }

        }

    }



    i=0;j=0;
    for(label1 in tiles){
      label1.tag =  array[i][j];
       // NSLog(@"%d,arr=%ld",i,array[i][j]);

       // NSLog(@"%d,tag=%ld",i,label1.tag);
        if(label1.tag!=0)
            [label1 setText:[NSString stringWithFormat:@"%ld",(long)label1.tag]];
        else
            [label1 setText:@""];

        j++;
        if(j%4==0){i++;j=0;}

    }

    [self randomize];



}

-(IBAction)movedown:(id)sender{
  int i=0,j=0,k=0;
  UILabel * label1=t00;
  long int array [4][4];

  for(label1 in tiles){


      array[i][j]=label1.tag;


      j++;
      if(j%4==0){i++;j=0;}


  }
  for(i=0;i<4;i++){
    for(j=0;j<4;j++){
      if(j<3&&array[j][i]!=0&&array[j+1][i]==0){
           array[j+1][i]=array[j][i];
           array[j][i]=0;
           k=j;
           while(k>0&&array[k-1][i]!=0){
             array[k][i]=array[k-1][i];
             array[k-1][i]=0;
             // NSLog(@"%d,arr=%ld",k,array[k][i]);

             k--;

           }

           //array[k+1][i]=0;


      }
      if(j<3&&array[j][i]==array[j+1][i]&&array[j][i]!=0){
          array[j+1][i]=2*array[j][i];

          sum.tag+=array[j+1][i];
          [sum setText:[NSString stringWithFormat:@"%ld",(long)sum.tag]];

          //sum.tag+=array[j+1][i];
          array[j][i]=0;
          k=j;
          while(k>0&&array[k-1][i]!=0){
            array[k][i]=array[k-1][i];
            array[k-1][i]=0;
            k--;

          }
          //array[k-1][i]=0;
      }


    }


  }
  i=0;j=0;
  for(label1 in tiles){
    label1.tag =  array[i][j];
     // NSLog(@"%d,arr=%ld",i,array[i][j]);

     // NSLog(@"%d,tag=%ld",i,label1.tag);
      if(label1.tag!=0)
          [label1 setText:[NSString stringWithFormat:@"%ld",(long)label1.tag]];
      else
          [label1 setText:@""];

      j++;
      if(j%4==0){i++;j=0;}

  }
  [self randomize];


}


@end
