//
//  ViewController.h
//  my_2048
//
//  Created by 米拉 on 2017/2/2.
//  Copyright © 2017年 米拉. All rights reserved.
//
#include <time.h>
#include <stdlib.h>
#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic,strong)IBOutlet UILabel * t00,*t01,*t02,*t03,
                                            * t10,*t11,*t12,*t13,
                                            * t20,*t21,*t22,*t23,
                                            * t30,*t31,*t32,*t33,
                                            *sum,*gameover,*score,*poster,*best;
@property (nonatomic,strong)IBOutlet UIButton *up,*down,*left,*right,*tryagain;
@property (nonatomic,strong) NSMutableArray * tiles;

@end
